'use strict';

buildGurusApp.controller('mainCtrl', ['$rootScope', '$state', function($rootScope, $state) {
	var mainCtrl = this;

	// add $state to $rootScope so that current page name is exposed
	$rootScope.$state = $state;

	mainCtrl.states = $state.get();
}]);