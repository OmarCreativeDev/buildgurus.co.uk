buildGurusApp.directive('collapseDropdown', function() {
	'use strict';
	return {
		restrict: 'A', //E = element, A = attribute, C = class, M = comment
		link: function (element) {
			angular.element(element).find('a[ui-sref]').click(function(){
				element.collapse('hide');
			});
		}
	};
});