buildGurusApp.directive('fancyBox', function() {
	'use strict';
	return {
		restrict: 'E', //E = element, A = attribute, C = class, M = comment
		templateUrl: '../views/common/fancyBox.html',
		scope: {
			imageCount: '@',
			imagePath: '@'
		},
		link: function (scope, elem) {
			// increment image count by 1 for array of file names
			// as image file names start from 1 and not 0
			scope.imageCount++;
			scope.collection = [];

			for(var i = 1; i < scope.imageCount; i++){
				scope.collection.push(i);
			}

			// invoke fancybox
			angular.element(elem).fancybox();
		}
	};
});
