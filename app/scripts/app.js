'use strict';

/**
 * @ngdoc overview
 * @name buildGurusApp
 * @description
 * # buildGurusApp
 *
 * Main module of the application.
 */
var buildGurusApp = angular.module('buildGurusApp', ['ngAnimate', 'ui.router']);

buildGurusApp.config(function ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('/', {
			templateUrl: 'views/home.html',
			url: '/',
			seoName: 'Home',
			camelCaseName: 'home'
		})
		.state('aboutUs', {
			templateUrl: 'views/aboutUs.html',
			url: '/about-us',
			seoName: 'About Us',
			camelCaseName: 'aboutUs'
		})
		/* services */
		.state('services', {
			templateUrl: 'views/services/services.html',
			url: '/services',
			seoName: 'Services',
			camelCaseName: 'services'
		})
		.state('plumbing', {
			templateUrl: 'views/services/plumbing.html',
			url: '/services/plumbing',
			seoName: 'Plumbing',
			camelCaseName: 'plumbing',
			isService: true
		})
		.state('electrical', {
			templateUrl: 'views/services/electrical.html',
			url: '/services/electrical',
			seoName: 'Electrical',
			camelCaseName: 'electrical',
			isService: true
		})
		.state('construction', {
			templateUrl: 'views/services/construction.html',
			url: '/services/construction',
			seoName: 'Construction',
			camelCaseName: 'construction',
			isService: true
		})
		.state('fencing', {
			templateUrl: 'views/services/fencing.html',
			url: '/services/fencing',
			seoName: 'Fencing',
			camelCaseName: 'fencing',
			isService: true
		})
		.state('gardening', {
			templateUrl: 'views/services/gardening.html',
			url: '/services/gardening',
			seoName: 'Gardening',
			camelCaseName: 'gardening',
			isService: true
		})
		.state('painting', {
			templateUrl: 'views/services/painting.html',
			url: '/services/painting',
			seoName: 'Painting',
			camelCaseName: 'painting',
			isService: true
		})
		.state('roofing', {
			templateUrl: 'views/services/roofing.html',
			url: '/services/roofing',
			seoName: 'Roofing',
			camelCaseName: 'roofing',
			isService: true
		})
		/* to do */
		.state('tiling', {
			templateUrl: 'views/services/tiling.html',
			url: '/services/tiling',
			seoName: 'Tiling',
			camelCaseName: 'tiling',
			isService: true
		})
		/* services */
		.state('contactUs', {
			templateUrl: 'views/contactUs.html',
			url: '/contact-us',
			seoName: 'Contact Us',
			camelCaseName: 'contactUs'
		});
});
